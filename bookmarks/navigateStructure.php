<?php
require_once ('include/db.php');

function getCategories ($parentid) {
	global $db;
	$sql = 'SELECT id, parentid, name, description, public FROM categories WHERE parentid=?';
	$sth = $db->prepare ($sql);
	$sth->execute (array ($parentid));
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}

function displayItem ($item) {
	echo "<li><a href='{$_SERVER['PHP_SELF']}?id={$item['id']}'>{$item['name']}</a>";
	if (isset($_GET['id'])&&$item['id']==$_GET['id'])
		echo (' (currently selected)');
	$subItems = getCategories($item["id"]);
	if (count($subItems)>0) {
		echo "<ul>";
		foreach ($subItems as $subItem) {
			displayItem ($subItem);
		}
		echo ("</ul>");
	}
}

$topLevel = getCategories(1);

echo "<ul>";

foreach ($topLevel as $item) {
	displayItem ($item);
}

echo "</ul>";
