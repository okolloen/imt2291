<?php
try {
	$db = new PDO('mysql:host=127.0.0.1;dbname=bookmarks','imt2291','imt2291');
} catch (PDOException $e) {
	die ('Unable to connect to database : '.$e->getMessage());
}

function getCategories ($parentid) {
	global $db;
	$sql = 'SELECT id, parentid, name, description, public FROM categories WHERE parentid=?';
	$sth = $db->prepare ($sql);
	$sth->execute (array ($parentid));
	return $sth->fetchAll(PDO::FETCH_ASSOC);
}


function displayItem ($item) {
	echo "<li>{$item["name"]}";
	$subItems = getCategories($item["id"]);
	if (count($subItems)>0) {
		echo "<ul>";
		foreach ($subItems as $subItem) {
			displayItem ($subItem);
		}
		echo ("</ul>");
	}
}

$topLevel = getCategories(1);


echo "<ul>";

foreach ($topLevel as $item) {
	displayItem ($item);
}
	
echo "</ul>";