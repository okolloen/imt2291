<?php
session_start();					// Start the session
require_once 'include/db.php';		// Connect to the database
require_once 'classes/user.php';	// Do login stuff

$pageTitle = "Welcome to the Bookmarks Database";
require_once 'include/heading.php';
?>

<div class="container">
<?php 
	if ($user->isLoggedIn())
		require_once ('include/welcomeUser.php');
?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>