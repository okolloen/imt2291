<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
</head>
<body>
<?php
/*
 * Parse a html file into a DOM
 * Use XPath to extract information
 */

$doc = new DOMDocument();
$doc->loadHTMLFile("http://www.hig.no");

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/meta");

if (!is_null($elements)) {
	foreach ($elements as $element) {
		for ($i=0; $i<$element->attributes->length; $i++) {
			$attr = $element->attributes->item($i);
			echo "{$attr->name}: {$attr->value}\t";	
		}
		echo "</br>\n";
	}
}