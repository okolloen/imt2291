<?php
/*
 * Scaling of images, output the scaled image to the browser or store it 
 * in a database.
 */
$content = file_get_contents("http://www.ebay.com/itm/EPSON-MOVERIO-BT-200-Smart-Glass-See-Through-Mobile-Viewer-/151518861624?pt=LH_DefaultDomain_0&hash=item23473a5938");
$doc = new DOMDocument();
$encoding = mb_detect_encoding($content);
$converted = mb_convert_encoding($content, "HTML-ENTITIES", $encoding);
@$doc->loadHTML($converted);

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/meta");

$images = array();

if (!is_null($elements)) {
	foreach ($elements as $element) {
		for ($i=0; $i<$element->attributes->length; $i++) {
			$attr = $element->attributes->item($i);
//			echo "{$attr->name}: {$attr->value}\t";
			if (strpos ($attr->value, 'image')!==false) {
				$attr1 = $element->attributes->item($i+1);
				@$images[$attr1->value] = 1;
			}
		}
//		echo "</br>\n";
	}
}

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/title");

if (!is_null($elements)) {
	foreach ($elements as $element) {
//		echo "Title: ".$element->nodeValue;
	}
}

$maxHeight = 200;
$maxWidth = 150;
header ("Content-type: image/jpg");

foreach ($images as $url => $nada) {
	$imgData = file_get_contents($url);
	$img = imagecreatefromstring($imgData);
	$width = imagesx($img);
	$height = imagesy($img);
	$scaleX = $width/$maxWidth;
	$scaleY = $height/$maxHeight;
	
	$scale = ($scaleX>$scaleY)?$scaleX:$scaleY;
	$scale = ($scale>1)?$scale:1;
	
	$scaled = imagecreatetruecolor($width/$scale, $height/$scale);
	imagecopyresampled($scaled, $img, 0, 0, 0, 0, $width/$scale, $height/$scale, $width, $height);
	imagejpeg ($scaled, null, 100);
	break;
}

/*
 * Save image to variable (to put it in a database.)
 */
// ob_start();		// http://php.net/manual/en/function.ob-start.php
// imagejpg...
// $image = ob_get_contents();
// ob_end_clean();