<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
</head>
<body>
<?php
/*
 * Many HTML documents contains information about images in the header
 * These images can be used to "represent" the document
 */
$content = file_get_contents("http://www.ebay.com/itm/EPSON-MOVERIO-BT-200-Smart-Glass-See-Through-Mobile-Viewer-/151518861624?pt=LH_DefaultDomain_0&hash=item23473a5938");
$doc = new DOMDocument();
$encoding = mb_detect_encoding($content);
$converted = mb_convert_encoding($content, "HTML-ENTITIES", $encoding);
$doc->loadHTML($converted);

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/meta");

$images = array();

if (!is_null($elements)) {
	foreach ($elements as $element) {
		for ($i=0; $i<$element->attributes->length; $i++) {
			$attr = $element->attributes->item($i);
			echo "{$attr->name}: {$attr->value}\t";
			if (strpos ($attr->value, 'image')!==false) {
				$attr1 = $element->attributes->item($i+1);
				$images[$attr1->value] = 1;
			}
		}
		echo "</br>\n";
	}
}

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/title");

if (!is_null($elements)) {
	foreach ($elements as $element) {
		echo "Title: ".$element->nodeValue;
	}
}

foreach ($images as $url => $nada)
	echo "</br><img src='$url'>";