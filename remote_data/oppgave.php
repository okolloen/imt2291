<form method="get" action="oppgave.php">
<label for="url">URL</label>
<input type="url" name="url" id="url" placeholder="http[s]://server.no/page.html" 
		title="Hvilken adresse skal jeg finne linker på?"/><br/>
<input type="submit" value="Finn lenker"/>
</form>
<?php
if (!isset($_GET['url'])) {
	die();
}

$content = file_get_contents($_GET['url']);
$doc = new DOMDocument();
$encoding = mb_detect_encoding($content);
$converted = mb_convert_encoding($content, "HTML-ENTITIES", $encoding);
@$doc->loadHTML($converted);

$xpath = new DOMXpath($doc);
$elements = $xpath->query("//a");

if (!is_null($elements)) {
	foreach ($elements as $element) {
		echo '<b>'.$element->nodeValue.'</b> : ';
		for ($i=0; $i<$element->attributes->length; $i++) {
			$attr = $element->attributes->item($i);
			if ($attr->name==='href')
			echo "{$attr->value}";	
		}
		echo "</br>\n";
	}
}