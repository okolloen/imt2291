<?php
/**
 * Create a database connection, all database information is stored here.
 * Note, iso encoded to allow usage by the file.php script
 */
try {
	$db = new PDO('mysql:host=127.0.0.1;dbname=file_library', 'root', 'imt2291');
} catch (PDOException $e) {
    die ('Kunne ikke koble til serveren : ' . $e->getMessage());
}
?>