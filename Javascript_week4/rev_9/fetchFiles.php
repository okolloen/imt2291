<?php 
/** 
 * Script used to get a list of files in a given "folder".
 * Get all public files and user files.
 */
 
// Return json encoded data
header ('Content-type: application/json');

session_start();
require_once 'db.php';

if (isset($_SESSION['user']))	// If a user is logged in, use the userid
	$user = $_SESSION['user'];
else							// If not, leave the userid blank
	$user = '';

// Return files in folder, either public files or all files if files belong to the user
$sql = 'SELECT files.id, files.name, mime, size, DATE_FORMAT(`date`, "%e/%c-%Y %k:%i") as `date`, description 
        FROM folders, files 
				WHERE folders.id=files.folderid AND folderid=? 
				AND (public="y" OR files.uid=?)';
$sth = $db->prepare ($sql);
$sth->execute (array ($_POST['id'], $user));	// Send the statement to the database
die (json_encode  ($sth->fetchAll ()));			// Get all files, json encode and return.
?>