<?php 
// We return json encoded data
header ('Content-type: application/json');

session_start();
require_once 'db.php';

if (!isset($_SESSION['user']))		// Can't remove a file if no user is logged in
	die (json_encode (array ('error'=>'No user logged on')));

$sql = 'DELETE FROM files where id=? and uid=?';		// Delete the given file for logged in user
$sth = $db->prepare ($sql);
$sth->execute (array($_POST['id'], $_SESSION['user']));	// Send the statement to the database
echo json_encode (array('ok'=>'OK'));					// No error checking, we just pretend everything is OK
?>