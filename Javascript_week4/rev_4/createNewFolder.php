﻿<?php
header ("Content-type: application/json");
session_start();
require_once 'db.php';

if (!isset($_SESSION['user']))
	die (json_encode (array ('error'=>'No user logged on')));

$sql = 'INSERT INTO folders (uid, name, parentid) VALUES (?, ?, ?)';
$sth = $db->prepare ($sql);
$res = $sth->execute (array ($_SESSION['user'], $_POST['name'], $_POST['parentId']));
if ($res==0)
	die (json_encode (array ('error'=>'Error during database operation')));
	
// Return folders for user
$sql = 'SELECT name, id, uid FROM folders WHERE uid=? and parentid=? order by name';
$sth = $db->prepare ($sql);
$sth->execute (array ($_SESSION['user'], $_POST['parentId']));
die (json_encode  ($sth->fetchAll ()));
?>