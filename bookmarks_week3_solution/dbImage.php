<?php
/**
 * This script is used to show thumbnail images
 * in the bookmarks listings. 
 * Call it with the id of the bookmark as a parameter
 * i.e. dbImage.php?id=...
 */
header ("Content-type: image/jpg");	// All thumbnails are stored as jpg images
require_once 'include/db.php';		// Connnect to the database

$sql = 'SELECT thumbnail FROM bookmarks WHERE id=?';
$sth = $db->prepare($sql);
$sth->execute (array ($_GET['id']));	// Get the image from the database
if ($row = $sth->fetch())				// If found
	echo $row['thumbnail'];				// Return the image
