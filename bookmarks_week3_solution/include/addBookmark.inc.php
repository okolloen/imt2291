<?php
/**
 * This script generates the form with details fetched from the 
 * url that the user wants to add as a bookmark.
 */
if (!isset($_GET['urlToInvestgate'])) {	// Make sure we are in the right place
	return;
}
$descriptions = array();	// Descriptions found
$titles = array();			// Titles found
$images = array ();			// Images found

// Get the content of the page
$content = file_get_contents($_GET['urlToInvestgate']);
$doc = new DOMDocument();

// Get encoding and convert, fixes ugly special character issues
$encoding = mb_detect_encoding($content);
$converted = mb_convert_encoding($content, "HTML-ENTITIES", $encoding);

// Parse the file into a dom
$doc->loadHTML($converted);

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/meta");	// Find all meta elements

if (!is_null($elements)) {				// meta information found
	foreach ($elements as $element) {	// Loop over all meta tags
		for ($i=0; $i<$element->attributes->length; $i++) {	// Loop over all attributes
			$attr = $element->attributes->item($i);
			if ($attr->name!='content' && strpos($attr->value, 'description')!==false) {
				// A description of the page found
				$data = $element->attributes->item($i+1);
				$descriptions[$data->value] = 1;
			}
			if ($attr->name!='content' && strpos($attr->value, 'title')!==false) {
				// A title for the page found
				$data = $element->attributes->item($i+1);
				$titles[$data->value] = 1;
			}
			if ($attr->name=='property' && strpos($attr->value, 'image')!==false) {
				// An image found
				$data = $element->attributes->item($i+1);
				$images[$data->value] = 1;
			}
		}
	}
}

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/title");	// Get the title of the page

if (!is_null($elements)) {						// Found it
	foreach ($elements as $element) {			// Add it to titles
		$titles[$element->nodeValue] = 1;
	}
} 

/*
 * We now have all the information about the URL, show a form
 * to the user enabling the user to select/edit the information
 * to store in the database.
 */
?>
<form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
<div class="form-group">
<label for="selectTitle">Page title</label>
<?php 
$index = 0;
$checked = ' checked="true"';
// Loop through all titles, let the user select/edit the title
// to store in the database
// NOTE: Auto select the first title, a bookmarks must have a title
foreach ($titles as $title => $dummy) { ?>
	<div class="col-xs-offset-1">
	<div class="radio">
	<input type="radio" name="selectTitle" value="<?php echo $index; ?>"<?php echo $checked; ?>>
	<input type="text" class="form-control" name="title[]" value="<?php echo $title; ?>">
	</div>
	</div>
	<?php 
	$checked = '';
	$index++;
}
echo '</div>';
echo '<div class="form-group">';
echo '<label for="selectDescription">Description</label>';
$index = 0;
$checked = ' checked="true"';
// Loop through the descriptions, let the user select/edit
// the description to store in the database.
// NOTE: Auto select the first description, a bookmark
// must have a description
foreach ($descriptions as $description => $dummy) { ?>
	<div class="col-xs-offset-1">
	<div class="radio">
	<input type="radio" name="selectDescription" value="<?php echo $index; ?>"<?php echo $checked; ?>>
	<textarea class="form-control" rows="3" name="description[]"><?php echo $description; ?></textarea></br>
	</div>
	</div>
	<?php 
	$checked = '';
	$index++;
}
echo '</div>';	
echo '<div class="form-group" style="margin-top:-20px">';
echo '<label for="selectThumbnail">Thumbnail image</label><br/>';
echo '<div class="col-xs-offset-1">';
$index = 0;
// Loop through the images, let the user choose an image
// to store as a thumbnail.
// NOTE: No image is auto selected, a bookmark can be stored
// without any thumbnails.
foreach ($images as $imageURL => $dummy) { ?>
	<label>
	<input type="radio" name="selectThumbnail" value="<?php echo $index; ?>">
	<input type="hidden" name="imageURL[]" value="<?php echo $imageURL; ?>"/>
	<img src="thumbnail.php?url=<?php echo urlencode($imageURL);?>" width="100"/>
	</label>
	<?php 
	$index++;
}	
echo '</div></div><div class="col-xs-offset-2" style="margin-top: 20px"><input class="btn btn-primary btn-sm" name="save" type="submit" value="Save bookmark"/> ';
echo '<input type="hidden" name="categoryID" value="'.$_GET['categoryID'].'"/>';
echo '<input type="hidden" name="url" value="'.$_GET['urlToInvestgate'].'"/>';
echo '<input class="btn btn-danger btn-sm" name="cancel" type="submit" value=" Cancel "/></form>';
echo '</div>';
