<!-- This form is shown when the user first chooses to add a bookmark.
Get the URL to add as a bookmark from the user. -->
<form class="form-inline" method="get" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
  <div class="form-group">
    <label class="sr-only" for="addURL">URL to add</label>
    <div class="input-group">
      <div class="input-group-addon"><span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span></div>
      <input type="url" name="urlToInvestgate" class="form-control" id="addURL" placeholder="URL to bookmark">
    </div>
  </div>
  <input type="hidden" name="categoryID" value="<?php echo $_GET['categoryID']; ?>"/>
  <button type="submit" class="btn btn-primary">Get information from URL</button>
</form>