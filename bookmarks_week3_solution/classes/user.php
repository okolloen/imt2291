<?php 
/**
 * The User class handles user login/logout and maintaining of login status.
 * The class accepts a database handle in the constructor and assumes there is
 * a table called users with the columns : id, email, pwd, givenname and surename.
 * For creating new users it is also assumed that the id field is auto incrementet
 * and that the email field has an index that enforces unique email (username).
 * 
 * @author imt2291
 *
 */
class User {
	var $uid = -1;
	var $name = NULL;
	var $db;
	var $rootId = -1;
	var $unknownUser = null;
	var $alert = null;
	
	/**
	 * The constructor accepts an object of PDO that contains the database connection.
	 * It is also assumed that the session is started before this file get included.
	 * Also note that $_SESSION, $_GET and $_POST needs to be of type superglobal.
	 * 
	 * The constructor checks for a $_GET variable of logout, if it exists the user
	 * is logget out.
	 * 
	 * If $_POST['email'] is set a logon is attempted. The password should then be
	 * in $_POST['password'].
	 * 
	 * if neither $_GET['logout'] nor $_POST['email'] is set then $_SESSION['uid']
	 * is checked. If this is set the user details (name of the user) is retrieved 
	 * for the database. If no user with id $_SESSION['uid'] exists the session 
	 * variable is cleared.
	 * 
	 * @param PDO $db
	 */
	function User ($db) {
		$this->db = $db;	// Store a refence to the database connection, not needed now, but maybe in the future
		if (isset($_GET['logout'])) {	// Logging out
			unset ($_SESSION['uid']);	// Clear the session variable
			if (isset($_COOKIE['persistent']))
				$this->removePersistentLogin();
		} else if (isset($_POST['email'])) {	// Logging in		
			$sql = "SELECT id, password, givenname, surename, root FROM users WHERE email=?";
			$sth = $db->prepare($sql);	
			$sth->execute (array ($_POST['email']));	// Get user info from the database
			if ($row = $sth->fetch(PDO::FETCH_ASSOC)) {	// If user exists
				if (password_verify ($_POST['password'], $row['password'])) {	// If correct password
					$_SESSION['uid'] = $row['id'];		// Store user id in session variable
					$this->uid = $row['id'];			// Store user id in object
					$this->name = array ('givenname'=>$row['givenname'], 'surename'=>$row['surename']);
					$this->rootId = $row['root'];
					if (isset($_POST['remember'])&&		// Logging in and remember login
							$_POST['remember']=='1')
						$this->persistentLogin();
				} else {	// Bad password
					$this->unknownUser = 'Uknown username/password';
					// Note, never say bad password, then you confirm the user exists
				}
			} else {		// Unknow user
				$this->unknownUser = 'Uknown username/password';
				// Same as for bad password
			}
		} else if (isset($_SESSION['uid'])) {	// A user is logged in
			$this->maintainSession();
		} else if (isset($_COOKIE['persistent'])&&$_COOKIE['persistent']!='') { //	Do autologin
			$this->autologin();
		}
	}

	function getAlertStatus () {
		return $this->alert;
	}
	
	/**
	 * Call this method when a user logs out and has persistent login
	 * enabled.
	 * This will remove the information about this persistent login
	 * session both from the user browser and from the database.
	 */
	function removePersistentLogin () {
		$loginData = split(":", $_COOKIE['persistent']);
		$uid = $loginData[0];
		$series = substr($loginData[1], 0, 32);
		
		$sql = 'DELETE from persistent_login WHERE uid=? and series=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($uid, $series));
		setcookie('persistent');
	}
	
	/**
	 * Called when successfully logged in and remember me is checked
	 * Creates a new row in the table persistent_login.
	 * 
	 * Note, this creates a new persistent login session
	 */
	function persistentLogin () {
		// Create an unique identifier for this login
		$headers = apache_request_headers ();
		$series = md5(session_id());
		
		// Create a unique identifier that will change for each 
		// subsequent auto login
		$token = md5($series.date(DATE_RFC2822).$this->uid.$headers['User-Agent']);
		
		$encryptedToken = password_hash ($token, PASSWORD_DEFAULT);
		
		// Store the data in the browser
		setcookie('persistent', "{$this->uid}:$series$token", strtotime( '+30 days' ));
		
		// Store the data in the database
		$sql = "INSERT INTO persistent_login (uid, series, token)
		VALUES (?, ?, ?)";
		
		
		$sth = $this->db->prepare ($sql);
		// Note, the token that changes is stored encrypted
		$sth->execute (array ($this->uid, $series, $encryptedToken));
	}
	
	/**
	 * This function is called if no other login exists but 
	 * an persistent login cookie was set.
	 * 
	 * Attempts an auto login. If uid and series id exists but
	 * the token does not match an intrucion alert will be raised 
	 * and all existing login information will be removed.
	 */
	function autoLogin () {
		$loginData = split(":", $_COOKIE['persistent']);
		if (count($loginData)==0)
			return;
		$uid = $loginData[0];
		$series = substr($loginData[1], 0, 32);
		$token = substr($loginData[1], 32, 32);
		// echo ("{$_COOKIE['persistent']}</br>$uid</br>$series</br>$token</br>");	// Used for debug purposes
		$sql = 'SELECT uid, series, token FROM persistent_login WHERE uid=? AND series=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array($uid, $series));
		if ($row = $sth->fetch()) {
			if (password_verify($token, $row['token'])) {
				$_SESSION['uid'] = $uid;
				$this->uid = $uid;

				// Create a unique identifier that will change for each
				// subsequent auto login
				$token = md5($series.date(DATE_RFC2822).$this->uid.$headers['User-Agent']);
				
				$encryptedToken = password_hash ($token, PASSWORD_DEFAULT);
								
				// Store new data in the browser
				setcookie('persistent', "{$this->uid}:$series$token", strtotime( '+30 days' ));
				
				// Update the data in the database
				$sql = "UPDATE persistent_login SET token=? WHERE uid=? AND series=?";
				
				$sth = $this->db->prepare ($sql);
				// Note, the token that changes is stored encrypted
				$sth->execute (array ($encryptedToken, $this->uid, $series));
				$this->maintainSession ();
			} else { // Userid/series id found but no matching token
				// This suggests a stolen cookie, some one has gained access 
				// as the user.
				$sql = "DELETE FROM persistent_login WHERE uid=?";
				
				$sth = $this->db->prepare ($sql);
				// Note, the token that changes is stored encrypted
				$sth->execute (array ($uid));
				$this->alert = "Possible security breach. All sessions terminated. You should log on and change your password.";
			}
		} // No userid/series identificator found, ignoring the information
	}
	
	/**
	 * This method is called if a $_SESSION['uid'] exists
	 * Used to get user information from the database.
	 */
	function maintainSession () {
		$sql = "SELECT givenname, surename, root FROM users WHERE id=?";
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($_SESSION['uid']));	// Find user information from the database
		if ($row = $sth->fetch()) {					// User found
			$this->uid = $_SESSION['uid'];			// Store user id in object
			$this->name = array ('givenname'=>$row['givenname'], 'surename'=>$row['surename']);
			$this->rootId = $row['root'];
		} else {									// No such user
			unset ($_SESSION['uid']);				// Remove user id from session
		}
	}
	
	/**
	 * Use this function to get the user id for the logged in user.
	 * Returns -1 if no user is logged in, or the id of the logged in user.
	 * 
	 * @return long integer with user id, or -1 if no user is logged in.
	 */
	function getUID() {
		return $this->uid;
	}
	
	/**
	 * Use this function to get the name of the user.
	 * Returns NULL if no user is logged in, an array with given name and 
	 * surename of a user is logged in.
	 * 
	 * @return array containing first and last name of user
	 */
	function getName() {
		return $this->name;
	}
	
	/**
	 * This method returns true if a user is logged in, false if no 
	 * user is logged in.
	 * 
	 * @return boolean value of true if a user is logged in, false if no user is logged in.
	 */
	function isLoggedIn() {
		return ($this->uid > -1);	// return true if userid > -1
	}
	
	/**
	 * This method  returns the id used as parentId for finding categories in 
	 * the "root folder" for this user.
	 * 
	 * @return integer id of the root folder for this user.
	 */
	function getRootId () {
		return $this->rootId;
	}
	
	/**
	 * This method inserts the HTML code for the login form when it is called.
	 * Note that any login attempt detected by the constructor will affect
	 * the outcome of this method. If a successfull login has been performed 
	 * this method return an empty string.
	 * If a failed login has been detected an alert box will be shown informing 
	 * the user of this fact. The user name will also be filled in with information 
	 * from the failed attempt.
	 * 
	 * This method also uses the calling script as recipient in the action attribute
	 * of the form.
	 * 
	 */
	function insertLoginForm () {
		if ($this->isLoggedIn())	// User is logged in
			return;					// Do not insert anything.
		$email = 'value=""';
		$firstname = 'value=""';
		$lastname = 'value=""';
		if (isset($this->unknownUser)) {	// If failed login
			// Set alert and email to be used in the form
			$this->alert = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span> '.$this->unknownUser.'</div>';
			$email = "value='{$_POST['email']}' ";
		}
		if (isset($_POST['lastname'])) {
			$lastname = 'value="'.$_POST['lastname'].'"';
			$firstname = 'value="'.$_POST['firstname'].'"';
		}
		require_once "user.loginform.inc.php";
	}
	
	/**
	 * Method used to add a user to the database. Takes username, password, first and
	 * last name as parameters and attempts to add the user to the database.
	 * 
	 * On success an array with the element 'success' is returned. If it
	 * failed (probably because the username was taken) an array with 
	 * two items is return, 'error' is set and 'description' gives the reason
	 * for the failure.
	 * 
	 * @param string $uname the username of the new user
	 * @param string $pwd the password for the new user
	 * @param string $givenname the first name of the new user
	 * @param string $surename the last name of the new user
	 * @return array that indicates failure or success. 
	 */
	function addUser ($uname, $pwd, $givenname, $surename) {
		$sql = 'INSERT INTO users (email, password, givenname, surename) VALUES (?, ?, ?, ?)';
	    $sth = $this->db->prepare ($sql);
	    // Add the user to the database
	    $sth->execute (array ($uname, password_hash($pwd, PASSWORD_DEFAULT), $givenname, $surename));
	    if ($sth->rowCount()==0) {	// If unable to create user
			$this->unknownUser = 'email address already registered';
	    	return (array ('error'=>'error', 'description'=>'email address already registered'));
	    }	
	    $this->uid = $this->db->lastInsertId();
	    $this->name = array ('givenname'=>$givenname, 'surename'=>$surename);
	    // Create root folder for user
	    $sql = 'INSERT INTO categories (parentid, name, public) VALUES (-1, "root", "n")';
	    if ($this->db->exec ($sql)==0) {	// If unable to create root folder (should never happen)
			$this->unknownUser = 'Unable to create root folder';
	    	return (array ('error'=>'error', 'description'=>'Unable to create root folder'));
	    }
	   	$this->rootId = $this->db->lastInsertId();
	    $sql = 'UPDATE users set root=? WHERE id=?';
	    $sth = $this->db->prepare ($sql);
	    // Update the root folder for user
	    $sth->execute (array ($this->rootId, $this->uid));
	    if ($sth->rowCount()==0) {		// If unable to set root folder for user (should never happen)
	    	$this->unknownUser = 'Unable set root folder for user';
	    	return (array ('error'=>'error', 'description'=>'Unable set root folder for user'));
	    }
		$_SESSION['uid'] = $this->uid;
	    return (array ('success'=>'success'));
	}
}

// Create an object of the User class, this also makes sure the constructor is called.
$user = new User($db);

