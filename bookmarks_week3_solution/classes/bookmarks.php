<?php
/**
 * This class handles some of the tasks regarding the actual bookmarks.
 * Displaying the forms for getting URLs for new bookmarks and
 * displaying forms for selecting/editing information for new 
 * bookmarks are handled separately. 
 * Storing bookmarks in the database and displaying lists of 
 * bookmarks are handled through this class.
 * @author imt2291
 *
 */
class Bookmarks {
	var $db;
	var $selectedCategory;
	var $justInsertedId = -1;
	
	/**
	 * The constructor checks to see if the user has pressed
	 * or save in the new bookmark form. If any of these actions
	 * is detected the appropriate action is taken.
	 * 
	 * If the user has pressed cancel the user is redirected to 
	 * the main page with the same category selected.
	 * It the user has pressed save the information about the 
	 * bookmark is stored in the database and then the user is
	 * taken to the main page where the new bookmark is highlighted.
	 * 
	 * @param PDO $db the database connection
	 * @param integer $rootID the id of the root category for this user
	 */
	function Bookmarks ($db, $rootID) {
		$this->db = $db;
		$this->selectedCategory = $rootID;
		if (isset ($_GET['categoryID']))	// Set the category
			$this->selectedCategory = $_GET['categoryID'];
		if (isset ($_GET['insertedBookmark']))	// A new bookmark was just stored
			$this->justInsertedId = $_GET['insertedBookmark'];
		if (isset($_POST['selectTitle'])&&
				isset($_POST['selectDescription'])&&
				isset($_POST['cancel'])) {	// The user canceled the creation of a new bookmark
			header ("Location: index.php?categoryID=".$_POST['categoryID']);
			die();
		} else if (isset($_POST['selectTitle'])&&
				isset($_POST['selectDescription'])&&
				isset($_POST['save'])) {	// The user want to save a new bookmark
			$thumbnail = null;
			if (isset($_POST['selectThumbnail'])) {	// Is a thumbnail image selected
				// Create an url to get the thumbnail image from the thumbnail.php script
				$host = 'http://'.$_SERVER['SERVER_ADDR'].':'.$_SERVER['SERVER_PORT'].$_SERVER['PHP_SELF'];
				$host = substr($host, 0, strrpos($host, "/"));
				$thumbnailurl =  $host.'/thumbnail.php?url='.urlencode($_POST['imageURL'][$_POST['selectThumbnail']]);
				$thumbnail = file_get_contents($thumbnailurl);	// Get the thumbnail image
			}
			$title = $_POST['title'][$_POST['selectTitle']];
			$description =$_POST['description'][$_POST['selectDescription']];
			$category = $_POST['categoryID'];
			$url = $_POST['url'];
			$sql = 'INSERT INTO bookmarks (categoryid, url, title, description, thumbnail) VALUES (?, ?, ?, ?, ?)';
			$sth = $this->db->prepare ($sql);
			// Add the bookmark to the database
			$sth->execute (array ($category, $url, $title, $description, $thumbnail));
			$id = $this->db->lastInsertId();	// Get the id of the newly created bookmark
			// Redirect to the main page, notify it of the newly created bookmark
			header ("Location: index.php?categoryID=".$_POST['categoryID'].'&insertedBookmark='.$id);
		}
	}
	
	/**
	 * Call this method at the location where you want the list of bookmarks
	 * for the currently selected category to be inserted.
	 */
	function insertBookmarks () {
		$sql = 'SELECT id, url, title, description, OCTET_LENGTH(thumbnail) AS thumbnailSize FROM bookmarks where categoryid=?';
		$sth = $this->db->prepare($sql);
		$sth->execute (array ($this->selectedCategory));	// Run the query
		$bookmarkdata = $sth->fetchAll(PDO::FETCH_ASSOC);	// Get all data from the database
		require_once 'include/bookmarkListing.inc.php';		// Let the code in this file produce the output
	}
}

// Create new object og hte Bookmarks class.
$bookmarks = new Bookmarks ($db, $user->getRootId());