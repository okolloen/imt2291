<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
</head>
<body>
<?php
if ($_GET['bookmarkpage']) {
	$descriptions = array();
	$titles = array();
	$images = array ();
	$content = file_get_contents($_GET['bookmarkpage']);
	$doc = new DOMDocument();
	$encoding = mb_detect_encoding($content);
	$converted = mb_convert_encoding($content, "HTML-ENTITIES", $encoding);
	$doc->loadHTML($converted);
	
	$xpath = new DOMXpath($doc);
	$elements = $xpath->query("/html/head/meta");
	
	if (!is_null($elements)) {
		foreach ($elements as $element) {
			for ($i=0; $i<$element->attributes->length; $i++) {
				$attr = $element->attributes->item($i);
				if ($attr->name!='content' && strpos($attr->value, 'description')!==false) {
					$data = $element->attributes->item($i+1);	
					$descriptions[$data->value] = 1;
				}
				if ($attr->name!='content' && strpos($attr->value, 'title')!==false) {
					$data = $element->attributes->item($i+1);	
					$titles[$data->value] = 1;
				}
				if ($attr->name=='property' && strpos($attr->value, 'image')!==false) {
					$data = $element->attributes->item($i+1);
					$images[$data->value] = 1;
				}
				echo "{$attr->name}: {$attr->value}\t";	
			}
			echo "</br>\n";
		}
	}
	
	$xpath = new DOMXpath($doc);
	$elements = $xpath->query("/html/head/title");
	
	if (!is_null($elements)) {
		foreach ($elements as $element) {
			$titles[$element->nodeValue] = 1;
		}
	} ?>
	<form method="post" action="task2.php">
	Title </br>
	<?php 
	$index = 0;
	$checked = ' checked="true"';
	foreach ($titles as $title => $dummy) { ?>
		<input type="radio" name="selectTitle" value="<?php echo $index; ?>"<?php echo $checked; ?>>
		<input type="text" name="title[]" value="<?php echo $title; ?>"></br>
		<?php 
		$checked = '';
		$index++;
	}
	echo 'Description </br>';
	$index = 0;
	$checked = ' checked="true"';
	foreach ($descriptions as $description => $dummy) { ?>
		<input type="radio" name="selectDescription" value="<?php echo $index; ?>"<?php echo $checked; ?>>
		<textarea name="description[]"><?php echo $description; ?></textarea></br>
		<?php 
		$checked = '';
		$index++;
	}	
	echo '<input type="submit" value="send"/></form>';
	echo 'Thumbnail image </br>';
	$index = 0;
		foreach ($images as $imageURL => $dummy) { ?>
			<input type="radio" name="selectThumbnail" value="<?php echo $index; ?>">
			<input type="hidden" name="imageURL[]" value="<?php echo $imageURL; ?>"/>
			<img src="thumbnail.php?url=<?php echo urlencode($imageURL);?>"/></br>
			<?php 
			$index++;
		}	
		echo '<input type="submit" value="send"/></form>';
} else if (isset($_POST['selectTitle'])) {
	echo "<pre>";
	print_r ($_POST);
	echo "</pre>";
}

echo "</br><pre>";
print_r ($titles);
print_r ($descriptions);
print_r ($images);
echo "</pre>";
?>
<form action="task2.php">
<input type="text" name="bookmarkpage" placeholder="URL to bookmark"/>
<input type="submit" value="check url"/>
</form>
