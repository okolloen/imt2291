﻿var latitude = null;
var longitude = null;
var geocoder;
var pick_map = null;
var pick_markers = new Array();
var pick_entryWindows = new Array();

function geo_success(p) {
	var pos=new google.maps.LatLng(p.coords.latitude,p.coords.longitude);
  geocoder.geocode({'latLng': pos}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[0]) {
				$('#left').append ('<p>Du later til å være logget på fra : <br/>'+results[0].formatted_address+'</p>');
      }
    } else {
			$('#left').append ('Beklager, men vi klarte ikke å finne ut hvor du befinner deg.');
    }
  });
	latitude = p.coords.latitude;
	longitude = p.coords.longitude;
}

function geo_error(p) {
	if (p.code==1)
		$('#left').append ('OK, du vil ikke dele din posisjon med andre.');
	else if (p.code==3)
		$('#left').append ('Vi gidder ikke vente lenger på å finne ut hvor du er.');
	else
		$('#left').append ('Vi fikk problemer med å fastslå din plassering.');
}		

$(document).ready (function () {
	geocoder = new google.maps.Geocoder();
	$('#newUserDialog').dialog({autoOpen:false, width: "500px", modal: true });
	$('#changeUserDetailsDialog').dialog({autoOpen:false, width: "700px", modal: true });
	$('#blogEntryDisplayDialog').dialog({autoOpen:false, width: "700px", modal: true });
	$.ajax({
		url: 'isLoggedIn.php',
		success: function (tmp) {
			data = eval ('('+tmp+')');
			if (data.login=='OK') {
				$('#left').load ('loginok.php');
				$('#content').load ('mineBloggInnlegg.php');
				if (geo_position_js.init()) {
					geo_position_js.getCurrentPosition(geo_success, geo_error, {timeout: 10000});
				}	else {
					$('#left').append ('Nettleseren din har ikke mulighet til å finne posisjonen din.');
				}
			} else {
				$('#content').load ('allBlogEntries.php');
			}
		}
	});
	$('#right').load('right.html');
});
		
function newBlogEntry () {
	$.ajax({
		url: 'newBlogEntry.html',
		success: function (data) {
			$('#content').html (data);
			$('#newBlogEntry').tinymce({
				language : 'en', 
				// Location of TinyMCE script
				script_url : '../tinymce/jscripts/tiny_mce/tiny_mce_gzip.php',

				// General options
				theme : "advanced",
				plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,advlist,spellchecker",

				// Theme options
				theme_advanced_buttons1 : "spellchecker,iespell,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,removeformat,help,code,|,visualchars,nonbreaking,visualaid,|,insertdate,inserttime",
				theme_advanced_buttons3 : "tablecontrols,|,sub,sup,|,charmap,emotions,media,advhr,|,print,|,fullscreen,|,cite,abbr,acronym,del,ins",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
			
				spellchecker_languages : "Norwegian=no,+English=en",
			});
		}
	});
}

function storeNewBlogEntry() {
	$.ajax({
		url: 'storeNewBlogEntry.php',
		data: {title: $('#content form input')[0].value, content: $('#newBlogEntry').html(), latitude: latitude, longitude: longitude },
		type: 'post',
		success: function (tmp) {
			data = eval ('('+tmp+')');
			if (data.ok) {
				alert (data.message);
				$('#content').load ('mineBloggInnlegg.php');
			} else
				alert (data.message);
		}
	});
}

function logOut () {
	$.ajax({
		url: 'logout.php',
		success: function (tmp) {
			$('#left').load ('login.html');
		}
	});
	$('#right').load('right.html');
	$('#content').load ('allBlogEntries.php');
};

function loggInn(form) {
	$.ajax({
		url: 'login.php',
		type: 'post',
		data: {'uname': form.uname.value, 'pwd': form.pwd.value},
		success: function (tmp) {
			data = eval ('('+tmp+')');
			if (data.ok == 'OK') {
				if(geo_position_js.init()) {
					geo_position_js.getCurrentPosition(geo_success,geo_error,{enableHighAccuracy:true});
				}	else {
					alert("Functionality not available");
				}
				$('#left').load ('loginok.php');
				$('#content').load ('mineBloggInnlegg.php');
			} else {
				$('#left div').first().show();
				$('#left input').first().get(0).focus();
			}
		}
	});
};

function newUserDialog () {
	$('#newUserDialog').dialog('open');
}

function changeUserDetailsDialog () {
	$.ajax ({
		url: 'userDetails.php',
		type: 'post',
		success: function (tmp) {
			data = eval ('('+tmp+')');
			if (data.error!=null) {
				alert (data.error);
				return;
			}
			var form = $('#changeUserDetailsDialog form').first()[0];
			form.uname.value = data.uid;
			form.uname.disabled = true;
			form.given.value = data.givenname;
			form.suren.value = data.surename;
			form.url.value = data.url;
			$('#changeUserDetailsDialog img').first()[0].src = 'userImage.php';
			$('#changeUserDetailsDialog').dialog('open');
		}
	});
}

function updateUserImage () {
	var d = new Date();
	$('#changeUserDetailsDialog img').first()[0].src = 'userImage.php?fix='+d;
	$('#left img').first()[0].src = 'userImage.php?fix='+d;
}

function newUser (form) {
	if (form.uname.value.length<6) {
		alert ("Brukernavnet må være minst 6 karakterer langt");
		form.uname.focus();
	} else if (form.pwd.value!=form.pwd1.value) {
		alert ("De to passordene må være like");
		form.pwd.focus();
	} else if (form.pwd.value.length<6) {
		alert ("Passordet må være minst 6 karakterer langt");
		form.pwd.focus();
	}
	$.ajax({
		url: 'newUser.php',
		type: 'post',
		data: { uname: form.uname.value, pwd: form.pwd.value, givenname: form.given.value, 
						surename: form.suren.value, url: form.url.value },
		success: function (tmp) {
			data = eval ('('+tmp+')');
			if (data.ok=="OK") {
				$.ajax({
					url: 'login.php',
					type: 'post',
					data: {'uname': form.uname.value, 'pwd': form.pwd.value},
					success: function (tmp) {
						$('#left').load ('loginok.php');
					}
				});
				$('#newUserDialog').dialog('close');
			} else {
				alert (data.message);
			}
		}
	});
}

function changeUserDetails (form) {
	if (form.pwd.value.length>0&&form.opwd.value.length<6) {
		alert ("Du må oppgi det gamle passordet for å sette nytt passord");
		form.opwd.focus();
	} else if (form.pwd.value!=form.pwd1.value) {
		alert ("De nye passordene dine matcher ikke");
		form.pwd.focus();
	} else if (form.pwd.value.length>0&&form.pwd.value.length<6) {
		alert ("Passord må være minst 6 karakterer langt");
		form.pwd.focus();
	}
	$.ajax({
		url: 'changeUserDetails.php',
		type: 'post',
		data: { uname: form.uname.value, opwd: form.opwd.value, pwd: form.pwd.value, 
						givenname: form.given.value, surename: form.suren.value, url: form.url.value },
		success: function (tmp) {
			data = eval ('('+tmp+')');
			alert (data.message);
		}
	});
}

function back (url) {
	$('#content').load (url);
	$('#right').load('right.html');
}

function showEntry (id, url) {
	$.ajax({
		url: 'getEntry.php',
		data: { id: id, returnURL: url },
		type: 'post',
		success: function (tmp) {
			data = eval ('('+tmp+')');
			if (data.ok) {
				$('#content').html (data.html);
				$('#right').html ('<img src="userImage.php?user='+data.uid+'"/><br/>&nbsp;<br/>');
				if (data.lat==data.lng==0) {
					$('#right').append ('Blog innlegget ble skrevet her : <div id="entry_map" style="width:150px; height:200px"></div>');
					var latlng = new google.maps.LatLng(data.lat, data.lng);
					var myOptions = {
						zoom: 8,
						center: latlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};
					var map = new google.maps.Map(document.getElementById("entry_map"), myOptions);
					var marker = new google.maps.Marker({
						position: latlng, 
						map: map, 
						title: 'Innlegget ble skrevet her'
					});
				}
			} else
				alert (data.message);
		}
	});
}

function chooseFromMap() {
	$('#right').load ('rightMenu.php');
	$('#content').html ('<h3>Dobbeltklikk i kartet under for å vise blog innlegg skrevet i nærheten</h3>');
	$('#content').append ('<div id="entry_pick_map" style="width:100%; height:400px"></div>');
	pick_markers = new Array();
	pick_entryWindows = new Array ();
	var latlng = new google.maps.LatLng(65, 10);
	var myOptions = {
		zoom: 4,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	pick_map = new google.maps.Map(document.getElementById("entry_pick_map"), myOptions);
	google.maps.event.addListener(pick_map, 'dblclick', function(event) {
		$.ajax({
			url: 'getClosestBlogEntries.php',
			data: { lat: event.latLng.lat(), lng: event.latLng.lng() },
			type: 'post',
			success: function (tmp) {
				data = eval ('('+tmp+')');
				for (i=0; i<data.length; i++) {
					var markerlatlng = new google.maps.LatLng(data[i].lat, data[i].lng);
					var marker = new google.maps.Marker({
						position: markerlatlng, 
						map: pick_map
					});
					pick_markers[pick_markers.length] = marker;
					var entries = '<ul class="google_map_entries_list">';
					for (j=0; j<data[i].innlegg.length; j++) {
						entries += data[i].innlegg[j];
					}
					entries += '</ul>';
					var blogEntriesWindow = new google.maps.InfoWindow({ content: entries });
					pick_entryWindows[pick_entryWindows.length] = blogEntriesWindow;
					google.maps.event.addListener(marker, 'click', function() {
						for (i=0; i<pick_markers.length; i++) {
							if (this.position==pick_markers[i].position)
								pick_entryWindows[i].open (pick_map, pick_markers[i]);
						}
					});
				}
			}
		});
  });
}

function showEntryDialog (id) {
	$.ajax ({
		url: 'getEntry.php',
		data: { id: id },
		type: 'post',
		success: function (tmp) {
			data = eval ('('+tmp+')');
			if (data.ok) {
				$('#blogEntryContents').html (data.html);
				$('#blogEntryDisplayDialog').dialog ('option', 'title', data.title);
				$('#blogEntryDisplayDialog').dialog ('open');
			}
		}
	});
}

function showAllEntries () {
	$('#content').load ('allBlogEntries.php');
	$('#right').load ('right.html');
}

function showMyEntries () {
	$('#content').load ('mineBloggInnlegg.php');
	$('#right').load ('right.html');
}