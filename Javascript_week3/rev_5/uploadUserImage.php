﻿<?php
session_start();
require_once 'db.php';
if (!isset($_SESSION['user']))
	die ('BOBO, ikke logget på');
$maxwidth = 140;
$maxheight = 400;
if (is_uploaded_file($_FILES['file']['tmp_name'])) {
    list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
    $factor1 = $height/$maxheight;
    $factor2 = $width/$maxwidth;
    $factor = ($factor1>$factor2)?$factor1:$factor2;
		if ($factor<1)
			$factor = 1;
    $new_width = $width / $factor;
    $new_height = $height / $factor;
    $image_p = imagecreatetruecolor($new_width, $new_height);
    $image = imagecreatefromstring(file_get_contents($_FILES['file']['tmp_name']));
    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    ob_start();
    imagepng($image_p);
    $imagevariable = ob_get_contents();
    ob_end_clean();
  }
  $sql = 'UPDATE users SET img=? WHERE uid=?';
  $sth = $db->prepare ($sql);
  $sth->execute (array ($imagevariable, $_SESSION['user']));
?><script type="text/javascript">
window.top.window.updateUserImage();
window.location = 'db.php'; // Tom, men fikser reload problematikk.
</script>