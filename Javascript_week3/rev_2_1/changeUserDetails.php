﻿<?php
/** 
 * This script is called from changeUserDetails in blogg.js
 * Store new user details for the user
 */

// This makes jQuery interpret returned data as json as default
header ('Content-type: application/json');

// Start the session handling system
session_start();
// Set up the database connection
require_once 'db.php';

// If the received username differs from the one stored in the session we make some noise
if ($_POST['uname']!=$_SESSION['user'])
	// This shouldn't happen and it means someone is trying to do bad things
	die (json_encode (array ('message'=>'Det er VELDIG dårlig gjort å prøve å lure systemet!')));

if (isset($_POST['pwd'])&&(strlen($_POST['pwd'])>0)) {	// If we're trying to change the password
	// First check to see if the given "old" password is correct
	$sql = 'SELECT * FROM users WHERE uid=? and pwd=?';
	$sth = $db->prepare ($sql);
	// Perform a "log in" operation to see if the old credentials is correct
	$sth->execute (array ($_SESSION['user'], md5($_POST['opwd'])));
	if ($row = $sth->fetch()) {	// Do nothing if everything is ok
	} else						// Give an error message if the old password doesn't match
		die (json_encode (array ('message'=>'Feil på det gamle passordet. Ingen oppdatering utført!')));
	$sth->fetchAll();		// Clear the statement, this enables us to reuse it
	// We can now update the password, note we assume that no one has manipulated the password
	// before posting it. This is not fail safe.
	$sql = 'UPDATE users SET pwd=? WHERE uid=? and pwd=?';
	$sth = $db->prepare ($sql);
	// Even now we double check and only set the new password when the old one matches
	$res = $sth->execute (array (md5($_POST['pwd']), $_SESSION['user'], md5($_POST['opwd'])));
	// Set a flag if the password was updated
	if ($sth->rowCount()==1)
		$pwdUpdated = 1;
}
// Update the other user information (givenname, surename and url)
$sql = 'UPDATE users SET givenname=?, surename=?, url=? WHERE uid=?';
$sth = $db->prepare ($sql);
$res = $sth->execute (array ($_POST['givenname'], $_POST['surename'], $_POST['url'], $_SESSION['user']));
if ($sth->rowCount()==1||(isset($pwdUpdated)))	// If data was updated
	echo json_encode (array ('message'=>'Oppdatering er lagret i databasen'));
else											// Nothing to update
	echo json_encode (array ('message'=>'Ingen oppdateringer'));
?>