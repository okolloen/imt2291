﻿<?php
/**
 * This script is called from document.ready in blogg.js to check if a user is logged in
 */
 
// This makes jQuery interpret returned data as json as default
header ('Content-type: application/json');

// Start the session handling system
session_start();
// Set up the database connection
require_once 'db.php';

// SQL statement to find a user based on the user id (email address)
$sql = 'SELECT * FROM users WHERE uid=?';
$sth = $db->prepare ($sql);
// Run the query, send the session variable "user" as a parameter
// If no session variable exists no row will be returned
$sth->execute (array ($_SESSION['user']));
if ($row=$sth->fetch()) {	// If a row is returned it means a user is logged in
	echo json_encode (array ('login'=>'OK'));
} else {					// If no row is returned it means that no user is logged in
	echo json_encode (array ('logon'=>'NOPE'));
}
?>