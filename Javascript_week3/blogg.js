﻿/**
 * This script file is included in the main file of our blog system.
 * All JavaScript code for the blog will be stored here.
 *
 * For now only the loggInn method is defined
 */
 
/**
 * The loggInn method is called when the user hits the logginn button
 * The form containing the username and password fields are send as a 
 * parameter to this method.
 * Use Ajax to call the login.php script to check if a user with the
 * given username and password exists.
 */
function loggInn(form) {
	$.ajax({
		url: 'login.php',	// Use the login.php script to check validity
		type: 'post',		// Use a POST request
		// Send username and password
		data: {'uname': form.uname.value, 'pwd': form.pwd.value},
		dataType: "json",
		success: function (data) {	// When the request is fullfilled
			if (data.ok == 'OK')	// OK shouldn't need further explanation
				$('#left').load ('loginok.php');	// Load loginok.php on the left
			else {		// Bad credentials was given
				// Show the div tag with the "Bad username/password" message
				$('#left div').first().show();
				// Put the focus back into the username field
				$('#left input').first().get(0).focus();
			}
		},
		error: function (jqXHR, status, error) {
			console.log(arguments);
			console.log (status);
			console.log (error);
		}
	});
};