﻿<?php
/**
 * Script used to check for the existence of a user with a given username and password
 * If a user is found the userid will be stored in a session variable
 */

 // This makes jQuery interpret returned data as json as default
 header ('Content-type: application/json');

// Start the session handling system
session_start();
// Set up the database connection
require_once 'db.php';

// Check for a user with the given username and password
$sql = 'SELECT * FROM users WHERE uid=? AND pwd=?';
$sth = $db->prepare ($sql);
// Perform the query
$sth->execute (array ($_POST['uname'], md5($_POST['pwd'])));

if ($row = $sth->fetch()) {		// If a row is returned that means we found the user
	// Store the username/user id in the session variable
	$_SESSION['user'] = $_POST['uname'];
	// Send a response indicating success
	echo json_encode (array ('ok'=>'OK'));
} else	// No row returned, this means no user found
	// Send a response indicating that the login failed
	echo json_encode (array ('bad_username'=>'No match for username/password'));
?>