﻿$(document).ready (function () {	// Run once the DOM is ready
	// Convert the DOM element with id=newUserDialog into a dialog
	$('#newUserDialog').dialog({autoOpen:false, width: "500px"});
	// Add click handler for the button in the dialog
	$('#newUserDialog input[type="button"]').click (function () {
		// When the user clicks the add user button call this javascript function from blogg.js
		newUser (this.form);
	});
	// Convert the DOM element with id=changeUserDetailsDialog into a dialog
	$('#changeUserDetailsDialog').dialog({autoOpen:false, width: "700px"});
	// Add a click handler to the button in the dialog
	$('#changeUserDetailsDialog input[type="button"]').click (function () {
		// When the user clicks the store changes button we call this javascript function that will
		// take care of storing the details.
		changeUserDetails(this.form);
	});
	$.ajax({	// Use Ajax to check if the user is already logged in, ie. on reload
		url: 'isLoggedIn.php',		// Script used to check login status, ie an active session
		success: function (data) {	// When the script completes
			if (data.login=='OK') {	// If a user is logged in
				$('#left').load ('loginok.php');	// Display the log out form 
			} else					// If no user is logged in
				$('#left').load ('login.html');		// Display the log in form
		}
	});
});

/**
 * The loggInn method is called when the user hits the logginn button
 * The form containing the username and password fields are send as a 
 * parameter to this method.
 * Use Ajax to call the login.php script to check if a user with the
 * given username and password exists.
 */
function loggInn(form) {
	$.ajax({
		url: 'login.php',	// Use the login.php script to check validity
		type: 'post',		// Use a POST request
		// Send username and password
		data: {'uname': form.uname.value, 'pwd': form.pwd.value},
		dataType: 'json',
		success: function (data) {	// When the request is fullfilled
			if (data.ok == 'OK')	// OK shouldn't need further explanation
				$('#left').load ('loginok.php');	// Load loginok.php on the left
			else {		// Bad credentials was given
				// Show the div tag with the "Bad username/password" message
				$('#left div').first().show();
				// Put the focus back into the username field
				$('#left input').first().get(0).focus();
			}
		}
	});
};

/**
 * Called when the "Registrer ny bruker" link is clicked
 * Opens the dialog that is used to register data about the new user
 */
function newUserDialog () {
	// Simply open the dialog that was created in blog.html when the DOM became ready
	$('#newUserDialog').dialog('open');
}

/**
 * This function will be called when the user clicks the "Endre brukerdata" link.
 * Call the userDetails.php script to get the user details and fill in the dialog.
 * Then let the user change those details.
 */
function changeUserDetailsDialog () {
	$.ajax ({					// Get existing data through Ajax
		url: 'userDetails.php',	// Use the script userDetails.php to get the details
		type: 'post',			// Use POST (no data is sent so could just as well use GET)
		success: function (data) {	// When the data is returned
			if (data.error!=null) {	// If an error message is returned
				alert (data.error);	// Display the error message
				return;				// and return, no further action is taken
			}
			// If we get here thenwe have the user data
			// First get a reference to the actual form DOM element
			var form = $('#changeUserDetailsDialog form').first()[0];
			form.uname.value = data.uid;	// Set user name
			form.uname.disabled = true;		// Disable this field, this can not be changed
			form.given.value = data.givenname;	// Set the given name
			form.suren.value = data.surename;	// Set the sure name
			form.url.value = data.url;			// Set the URL for the user homepage
			// Display the current user image (avatar image)
			$('#changeUserDetailsDialog img').first()[0].src = 'userImage.php';
			// Open the dialog
			$('#changeUserDetailsDialog').dialog('open');
		}
	});
}

/**
 * Function is used to reload the user image both on the main webpage and in the user details
 * dialog.
 * Note that we need to apply a fix since we reload using an existing URL, so we append
 * the current timestamp as a parameter.
 */
function updateUserImage () {
	// Get the current timestamp
	var d = new Date();
	// Set the source of the user details dialog image
	$('#changeUserDetailsDialog img').first()[0].src = 'userImage.php?fix='+d;
	// Set the source of the user image on the main page
	$('#left img').first()[0].src = 'userImage.php?fix='+d;
}

/**
 * Called when clicking the button to create a new user in the dialog opened above
 * This function will check the minimum requirements, ie. username and equal passwords
 * the data is then stored in the database using the newUser.php script. 
 * If OK is returned from this script the login.php script is called to automaticaly 
 * log in with the newly created user.
 * If an error is returned this error message is displayed to the user with a standard 
 * javascript alert message box.
 */
function newUser (form) {
	if (form.uname.value.length<6) {	// The username need to be at least six characters
		alert ("Brukernavnet må være minst 6 karakterer langt");
		form.uname.focus();				// If bad username, put the focus on the username field
	} else if (form.pwd.value!=form.pwd1.value) {	// The passwords must be equal
		alert ("De to passordene må være like");
		form.pwd.focus();							// Put the focus on the first password field
	} else if (form.pwd.value.length<6) {			// The passwords must be at least six characters
		alert ("Passordet må være minst 6 karakterer langt");
		form.pwd.focus();							// Put the focus on the password field
	}
	$.ajax({					// Data is valid, post the new user details
		url: 'newUser.php',		// Use the script newUser.php
		type: 'post',			// Use a post request
		// Add username, password, given and surename and the user homepage url
		data: { uname: form.uname.value, pwd: form.pwd.value, givenname: form.given.value, 
						surename: form.suren.value, url: form.url.value },
		dataType: 'json',
		success: function (data) {		// When the request is completed
			if (data.ok=="OK") {		// If everything went well
				$.ajax({				// Use Ajax again to log in as the newly create user
					url: 'login.php',	// Use the login.php script
					type: 'post',
					// We have the username and password
					data: {'uname': form.uname.value, 'pwd': form.pwd.value},
					success: function (data) {
						// Just assume that this is OK, after all the user was just created
						$('#left').load ('loginok.php');
					}
				});
				// We are now logged in, so close the create new user dialog
				$('#newUserDialog').dialog('close');
			} else {	// Something prevented us from creating the new user
				// Show the error message returned from the newUser.php script
				alert (data.message);
			}
		}
	});
}

/**
 * This function is called to store changes in the user details from the change user details dialog
 * A simple data validity check is performed, ie. if a password change is requested the old password
 * must be given and the new passwords must match and be at least six characters long.
 * Use the changeUserDetails.php script to actually store the changes in the database.
 */
function changeUserDetails (form) {
	// If a new password is given but the old password is shorter than 6 characters
	if (form.pwd.value.length>0&&form.opwd.value.length<6) {
		// Alert the user to this mistake
		alert ("Du må oppgi det gamle passordet for å sette nytt passord");
		// Then focus on the old password field
		form.opwd.focus();
	} else if (form.pwd.value!=form.pwd1.value) {	// If the two new passwords don't match
		// Alert the user to the error
		alert ("De nye passordene dine matcher ikke");
		// Focus on the first of the two new password fields
		form.pwd.focus();
	} else if (form.pwd.value.length>0&&form.pwd.value.length<6) {	// New password is to short
		// Alert the user to the mistake
		alert ("Passord må være minst 6 karakterer langt");
		// Focus on the first of the two new password fields
		form.pwd.focus();
	}
	$.ajax({	// Store the new information using the changeUserDetails.php script
		url: 'changeUserDetails.php',
		type: 'post',	// Use a POST request
		data: { uname: form.uname.value, opwd: form.opwd.value, pwd: form.pwd.value, 
						givenname: form.given.value, surename: form.suren.value, url: form.url.value },
		success: function (data) {	// When the request is completed
			alert (data.message);	// Show the result of the operation to the user
			// Note that the dialog remains open, if we wanted it to be closed that could be
			// accompplished by the javascript line :
			// $('#changeUserDetailsDialog').dialog('open');
		}
	});
}