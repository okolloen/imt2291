﻿$(document).ready (function () {	// Run once the DOM is ready
	$.ajax({	// Use Ajax to check if the user is already logged in, ie. on reload
		url: 'isLoggedIn.php',		// Script used to check login status, ie an active session
		dataType: 'json',
		success: function (data) {	// When the script completes
			if (data.login=='OK') {	// If a user is logged in
				$('#left').load ('loginok.php');	// Display the log out form 
			} else					// If no user is logged in
				$('#left').load ('login.html');		// Display the log in form
		}
	});
});

/**
 * The loggInn method is called when the user hits the logginn button
 * The form containing the username and password fields are send as a 
 * parameter to this method.
 * Use Ajax to call the login.php script to check if a user with the
 * given username and password exists.
 */
function loggInn(form) {
	$.ajax({
		url: 'login.php',	// Use the login.php script to check validity
		type: 'post',		// Use a POST request
		// Send username and password
		data: {'uname': form.uname.value, 'pwd': form.pwd.value},
		dataType: 'json',
		success: function (data) {	// When the request is fullfilled
			if (data.ok == 'OK')	// OK shouldn't need further explanation
				$('#left').load ('loginok.php');	// Load loginok.php on the left
			else {		// Bad credentials was given
				// Show the div tag with the "Bad username/password" message
				$('#left div').first().show();
				// Put the focus back into the username field
				$('#left input').first().get(0).focus();
			}
		}
	});
};

/**
 * Called when the "Registrer ny bruker" link is clicked
 * Opens the dialog that is used to register data about the new user
 */
function newUserDialog () {
	// Simply open the dialog that was created in blog.html when the DOM became ready
	$('#newUserDialog').dialog('open');
}

/**
 * Called when clicking the button to create a new user in the dialog opened above
 * This function will check the minimum requirements, ie. username and equal passwords
 * the data is then stored in the database using the newUser.php script. 
 * If OK is returned from this script the login.php script is called to automaticaly 
 * log in with the newly created user.
 * If an error is returned this error message is displayed to the user with a standard 
 * javascript alert message box.
 */
function newUser (form) {
	if (form.uname.value.length<6) {	// The username need to be at least six characters
		alert ("Brukernavnet må være minst 6 karakterer langt");
		form.uname.focus();				// If bad username, put the focus on the username field
	} else if (form.pwd.value!=form.pwd1.value) {	// The passwords must be equal
		alert ("De to passordene må være like");
		form.pwd.focus();							// Put the focus on the first password field
	} else if (form.pwd.value.length<6) {			// The passwords must be at least six characters
		alert ("Passordet må være minst 6 karakterer langt");
		form.pwd.focus();							// Put the focus on the password field
	}
	$.ajax({					// Data is valid, post the new user details
		url: 'newUser.php',		// Use the script newUser.php
		type: 'post',			// Use a post request
		// Add username, password, given and surename and the user homepage url
		data: { uname: form.uname.value, pwd: form.pwd.value, givenname: form.given.value, 
						surename: form.suren.value, url: form.url.value },
		dataType: 'json',
		success: function (data) {		// When the request is completed
			if (data.ok=="OK") {		// If everything went well
				$.ajax({				// Use Ajax again to log in as the newly create user
					url: 'login.php',	// Use the login.php script
					type: 'post',
					// We have the username and password
					data: {'uname': form.uname.value, 'pwd': form.pwd.value},
					success: function (data) {
						// Just assume that this is OK, after all the user was just created
						$('#left').load ('loginok.php');
					}
				});
				// We are now logged in, so close the create new user dialog
				$('#newUserDialog').dialog('close');
			} else {	// Something prevented us from creating the new user
				// Show the error message returned from the newUser.php script
				alert (data.message);
			}
		}
	});
}