﻿<?php
/**
 * This code will be loaded into the left hand side of the browser when a user is logged in
 */
 
// Start the session handling system
session_start();
// Set up the database connection
require_once 'db.php';
// Get the user from the users table based on the user id stored in the session variable
$sql = 'SELECT * FROM users WHERE uid=?';
$sth = $db->prepare ($sql);
// Execute the query, send the user id from the session as a parameter
// Note, if no session variable is set or an illegal user id is stored in the session
// this will mean we get no return result
$sth->execute (array ($_SESSION['user']));
if ($row=$sth->fetch()) { // If we get a result, that means we are logged on ?>
<!-- User is logged on, we give a warm welcome and display a link to change the user details
as well as a button to logg out again -->
<b>Velkommen<br/><?php echo $row['givenname']; ?> <?php echo $row['surename']; ?></b><br>
	<a href="javascript:changeUserDetailsDialog()">Endre brukerdata</a><br/>
	<input type="button" value="Logg ut" onclick="javascript:logOut();"/>
	<?php
} else {
	// This is bad, no user is logged in.
	echo "Du er jo ikke logget inn!?!?!";
}
?>
	