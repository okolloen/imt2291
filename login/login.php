<?php
try {
	$db = new PDO('mysql:host=127.0.0.1;dbname=testing','imt2291','imt2291');
} catch (PDOException $e) {
	die ('Unable to connect to database : '.$e->getMessage());
}

if (isset($_POST['email'])) {
	$sql = "SELECT id FROM users WHERE email=? AND pwd=?";
	$sth = $db->prepare($sql);
	$res = $sth->execute (array ($_POST['email'], MD5($_POST['password'])));
	if ($row = $sth->fetch()) {
		session_start();
		$SESSION['uid'] = $row['id'];
		die ('You are logged in');
	} else {
		$unknownUser = 'Uknown username/password';
	}
}

require_once 'classes/user.php';
$user = new User($db);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Login example</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
<link rel="stylesheet" href="signin.css"/>
</head>
<body>
<div class="container">
	<form class="form-signin" method="post">
		<h2 class="form-signin-heading">Please sign in</h2>
		<?php
		if (isset ($unknownUser)) 
			echo '<div class="alert alert-danger" role="alert">'.$unknownUser.'</div><br/>';
		?>
		<label for="inputEmail" class="sr-only">Email address</label>
		<input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required autofocus/>
		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required/>
		<div class="checkbox">
			<label>
				<input type="checkbox" value="remember-me"> Remember me
			</label>
		</div>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	</form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<?php 
$user->checkPasswordFunctions("kalleKlovn");
?>
</body>
</html>