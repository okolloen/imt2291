<?php
/**
 * This class is responsible for everything conserning the display and
 * handling of the categories. 
 * Contains functionality for inserting the category tree, displaying
 * add/edit/delete forms and for the actuall adding, editing and deleting
 * of categories.
 * 
 * @author imt2291
 *
 */
class Categories {
	var $db;
	var $selected=-1;
	var $rootId;
	var $description;
	var $name;
	var $public;
	
	/**
	 * If adding, editing og deleting of a category is detected
	 * the operation is performed before a redirect to index.php
	 * is performed.
	 * If no adding, editing or deletion is requested then 
	 * the name, description and public status is fetched from the database.
	 * 
	 * @param PDO $db a database connection
	 * @param integer $rootId the id of the root category for this user
	 */
	function Categories($db, $rootId) {
		$this->db = $db;
		if (isset($_POST['addCategoryName']))		// Add a category
			$this->addCategory ();
		else if (isset($_POST['editCategoryName']))	// Edit a category
			$this->editCategory ();
		else if (isset($_POST['deleteid']))			// Delete a category
			$this->deleteCategory ();
		$this->selected = $rootId;
		if (isset($_GET['categoryID']))				// A category is selected
			$this->selected = $_GET['categoryID'];
		$this->rootId = $rootId;					// Root id for this user
		$sql = 'SELECT name, description, public from categories WHERE id=?';
		$sth = $this->db->prepare ($sql);
		$sth->execute (array($this->selected));
		if ($row = $sth->fetch()) {	// Category found
			$this->name = $row['name'];
			$this->description = $row['description'];
			$this->public = $row['public']=='y';	// public is stored as y/n in database, boolean true/false in the object
		}
	}

	/**
	 * Use this function to insert the add category form 
	 * into the HTML document being generated.
	 */
	function insertAddCategoryForm () {
		require ('categories.newcategoryform.inc.php');
	}
	
	/**
	 * Use this function to insert the edit category form 
	 * into the HTML document being generated.
	 */
	function insertEditCategoryForm () {
		require ('categories.editcategoryform.inc.php');
	}

	/**
	 * Use this function to insert the delete category form 
	 * into the HTML document being generated.
	 */
	function insertDeleteCategoryForm () {
		require ('categories.deletecategoryform.inc.php');
	}
	
	/**
	 * Use this function to insert the category tree structure
	 * into the HTML document being generated.
	 */
	function insertCategoriesTree () {
		$topLevel = $this->getCategories($this->rootId);	// Get items at root level		
		echo '<ul  class="nav nav-pills nav-stacked nav-tree" id="categories" data-toggle="nav-tree">';
		foreach ($topLevel as $item) {		// Loop through items at root level
			$this->displayItem ($item);		// Output code for all items
		}
		echo "</ul>";
	}
	
	/**
	 * This function is called from the constructor to add a
	 * new category
	 */
	private function addCategory () {
		$sql = "INSERT INTO categories (parentid, name, description, public) VALUES (?, ?, ?, ?)";
		$sth = $this->db->prepare ($sql);
		if (isset($_POST['public'])&&$_POST['public']=='y')	// "public" is stored as y/n in the database
			$public = 'y';
		else
			$public = 'n';
		$sth->execute (array ($_POST['parentID'], $_POST['addCategoryName'],
				$_POST['description'], $public));
		$id = $this->db->lastInsertId();					// Get id for newly created category
		header ("Location: index.php?categoryID=$id");		// Show the bookmarks database, select the newly created category
		die();		// Stop script execution
	}
	
	/**
	 * This function is called from the constructor to update a
	 * category.
	 */
	private function editCategory () {
		$sql = "UPDATE categories SET name=?, description=?, public=? WHERE id=?";
		$sth = $this->db->prepare ($sql);
		if (isset($_POST['public'])&&$_POST['public']=='y')	// "public" is stored as y/n in the database
			$public = 'y';
		else
			$public = 'n';
		$sth->execute (array ($_POST['editCategoryName'], $_POST['description'],
				$public, $_POST['id']));
		header ("Location: index.php?categoryID=".$_POST['id']);	// Show the bookmarks database, select the updated category
		die();		// Stop script execution
	}
	
	/**
	 * This function is called from the constructor to remove
	 * a category.
	 */
	private function deleteCategory () {
		$sql = "SELECT parentid FROM categories WHERE id=?";
		$sth = $this->db->prepare ($sql);
		$sth->execute (array ($_POST['deleteid']));	// Get the id of the parent of the category being deleted
		if ($row = $sth->fetch()) {
			$sql = "DELETE FROM categories WHERE id=?";
			$sth = $this->db->prepare ($sql);
			$sth->execute (array ($_POST['deleteid']));	// Delete category
			header ("Location: index.php?categoryID=".$row['parentid']);	// Show the bookmarks database, select the category that was the parent of the category being deleted
			die();	// Stop script execution
		}
		header ("Location: index.php?categoryID=".$_POST['deleteid']);	// Unable to delete (no parent????)
		die();
	}
	
	/**
	 * Get categories with a given parent id.
	 * 
	 * @param integer $parentid the parent id for which to fetch the child categories.
	 * @return an array with category elements
	 */
	private function getCategories ($parentid) {
		$sql = 'SELECT id, parentid, name, description, public FROM categories WHERE parentid=?';
		$sth = $this->db->prepare ($sql);
		$sth->execute (array ($parentid));
		return $sth->fetchAll(PDO::FETCH_ASSOC);	// Return the array with all information
	}
	
	/**
	 * Add HTML for this item and sub items.
	 * 
	 * @param array $item containing id, parentid, name and description
	 */
	private function displayItem ($item) {
		$active = "";
		if ($this->selected==$item['id']) {	// If this is the selected category, add class="active"
			$active = ' class="active"';
		}
		// Output each category as a link in a list item
		echo "<li$active><a href='{$_SERVER['PHP_SELF']}?categoryID={$item['id']}'>{$item['name']}</a>";
		$subItems = $this->getCategories($item["id"]);	// Get child categories
		if (count($subItems)>0) {	// Has child categories
			echo '<ul class="nav nav-pills nav-stacked nav-tree">';
			foreach ($subItems as $subItem) {	// Loop through child categories
				$this->displayItem ($subItem);	// Output child category
			}
			echo ("</ul>");
		}
	}
}

// Create object of the Categories class
$categories = new Categories ($db, $user->getRootId());