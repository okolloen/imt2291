<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $pageTitle; ?></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
<link rel="stylesheet" href="BootstrapTreeNav/dist/css/bootstrap-treenav.min.css"/>
<link rel="stylesheet" href="css/bookmarks.css"/>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Bookmarks DB</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Edit <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="addCategory.php?categoryID=<?php echo $categories->selected; ?>">Add category</a></li>
            <li><a href="editCategory.php?categoryID=<?php echo $categories->selected; ?>">Edit category</a></li>
            <li><a href="deleteCategory.php?categoryID=<?php echo $categories->selected; ?>">Delete category</a></li>
            <li class="divider"></li>
            <li><a href="#">Add bookmark</a></li>
            <li><a href="#">Edit bookmark</a></li>
            <li><a href="#">Delete bookmark</a></li>
            <li class="divider"></li>
            <li><a href="#">Update user profile</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
		<li>
			<form class="navbar-form navbar-left" role="search">
	        <div class="form-group">
	          <input type="text" class="form-control" placeholder="Search">
	        </div>
	        <button type="submit" class="btn btn-default">Submit</button>
	 	    </form>
		</li>
      	<?php 
      		if ($user->isLoggedIn())
      			echo '<a href="index.php?logout=true" class="btn btn-default navbar-btn">Sign out</a>';
      		else
      			echo '<a href="signin.php" class="btn btn-default navbar-btn">Sign in</a>'
      	?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>