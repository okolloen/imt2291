<?php
session_start();					// Start the session
require_once 'include/db.php';		// Connect to the database
require_once 'classes/user.php';	// Do login stuff

// Create a new user
$res = $user->addUser ('imt2291@hig.no', 'imt2291', 'imt2291', 'www-teknologi');
if (isset($res['success']))		// Successfully created new user
	echo "New user created";
else							// Unable to create user
	echo $res['description'];
