<?php
require_once 'db.php';
header("Content-Type: application/json");

if (isset($_POST['text'])&&!isset($_POST['id'])) {
  $sql = "insert into demo (text) values(?)";
  $stmt = $db->prepare ($sql);
  $rows = $stmt->execute(array($_POST['text']));
  if ($rows==1) {
    echo json_encode(array("status"=>"success", "id"=>$db->lastInsertId()));
  } else {
    echo json_encode(array("status"=>"fail", "message"=>$stmt->errorInfo()[2]));
  }
} else if (isset($_POST['text'])&&isset($_POST['id'])) {
  $sql = "UPDATE demo set text=? where id=?";
  $stmt = $db->prepare ($sql);
  $rows = $stmt->execute(array($_POST['text'], $_POST['id']));
  if ($rows==1) {
    echo json_encode(array("status"=>"success", "id"=>$_POST['id']));
  } else {
    echo json_encode(array("status"=>"fail", "message"=>$stmt->errorInfo()[2]));
  }
} else if (!isset($_POST['text'])&&isset($_POST['id'])) {
  $sql = "DELETE FROM demo where id=?";
  $stmt = $db->prepare ($sql);
  $rows = $stmt->execute(array($_POST['id']));
  if ($rows==1) {
    echo json_encode(array("status"=>"success"));
  } else {
    echo json_encode(array("status"=>"fail", "message"=>$stmt->errorInfo()[2]));
  }
}
