<?php
header ("Content-type: application/javascript");
$lines = file ('HiG-studenter-2014.tsv');
$data = array();
foreach ($lines as $line) {
	$data[] = split (",", $line);
}
if (!isset($_GET['aggregate']))
	echo "var data = ".json_encode ($data);
else if ($_GET['aggregate']=='postnummer') {
	$data1 = array();
	foreach ($data as $idx=>$student) {
		if ($idx==0)
			continue;
		if (isset($data1[$student[4]]))
			$data1[$student[4]][] = $student;
		else {
			$data1[$student[4]] = array ();
			$data1[$student[4]][] = $student;
		}
	}
	foreach ($data1 as $data) {
		$lat = 0;
		$lon = 0;
		$i = 0;
		foreach ($data as $student) {
			$lat += $student[5];
			$lon += $student[6];
			$i++;
		}
		$lat = $lat/$i;
		$lon = $lon/$i;
			foreach ($data as $student) {
			$student[5] = $lat;
			$student[6] = $lon;
		}
	}
	$data2 = array ();
	foreach ($data1 as $data) {
		foreach ($data as $student) {
			$data2[] = $student;
		}
	}
	echo "var data = ".json_encode ($data2);
} else if ($_GET['aggregate']=="fylke") {
	$fylkesnr = array (
		'01'=> 'Østfold',
		'02'=> 'Akershus',
		'03'=> 'Oslo',
		'04'=> 'Hedmark',
		'05'=> 'Oppland',
		'06'=> 'Buskerud',
		'07'=> 'Vestfold',
		'08'=> 'Telemark',
		'09'=> 'Aust-Agder',
		'10'=> 'Vest-Agder',
		'11'=> 'Rogaland',
		'12'=> 'Hordaland',
		'14'=> 'Sogn og Fjordane',
		'15'=> 'Møre og Romsdal',
		'16'=> 'Sør-Trøndelag',
		'17'=> 'Nord-Trøndelag',
		'18'=> 'Nordland',
		'19'=> 'Troms',
		'20'=> 'Finmark',
		'21'=> 'Svalbard',
		'22'=> 'Jan Mayen',
		'23'=> 'Kontinentalsokkelen');
} else if ($_GET['aggregate']=='kommune') {
	$lines1 = file ('postnr.txt');
	$postnr = array();
	foreach ($lines1 as $line) {
		$post = split ("\t", $line);
		$postnr[$post[0]*1] = $post[2];
	}
	$data1 = array();
	foreach ($data as $idx=>$student) {
		if ($idx==0)
			continue;
		if (isset($data1[$postnr[$student[4]]]))
			$data1[$postnr[$student[4]]][] = $student;
		else {
			$data1[$postnr[$student[4]]] = array ();
			$data1[$postnr[$student[4]]][] = $student;
		}
	}
	$data2 = array ();
	foreach ($data1 as $data) {
		$lat = 0;
		$lon = 0;
		$i = 0;
		foreach ($data as $student) {
			$lat += $student[5];
			$lon += $student[6];
			$i++;
		}
		$lat = $lat/$i;
		$lon = $lon/$i;
		foreach ($data as $student) {
			$student[5] = $lat;
			$student[6] = $lon;
			$data2[] = $student;
		}
	}
	echo "var data = ".json_encode ($data2);
}