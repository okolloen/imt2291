<?php
	session_start();
	require_once 'db.php';
	
	if (!isset($_SESSION['user']))	// Must be logged in to do anything
		die (json_encode (array ('message'=>'Du kan ikke lagre blog innlegg når du ikke er logget på')));
	if (isset($_POST['latitude'])&&$_POST['latitude']!='') {	// New lat/lng given
		$lat = $_POST['latitude'];
		$lng = $_POST['longitude'];
		$sql = 'UPDATE entry SET title=?, entry=?, lat=?, lng=?, `when`=now() WHERE uid=? AND id=?';
		$sth = $db->prepare ($sql);
		$sth->execute (array ($_POST['title'], $_POST['content'], 
				$_POST['latitude'], $_POST['longitude'], 
				$_SESSION['user'], $_POST['id']));
	} else {	// No new lat/lng given, don't change the location
		$sql = 'UPDATE entry SET title=?, entry=?, `when`=now() WHERE uid=? AND id=?';
		$sth = $db->prepare ($sql);
		$sth->execute (array ($_POST['title'], $_POST['content'],
				$_SESSION['user'], $_POST['id']));
	}		
	if ($sth->rowCount()==1)	// One row affected, that means everything is OK
		die (json_encode (array('ok'=>'OK', 'message'=>'Endringer i blogg innlegget er lagret i databasen')));
	else // No rows affected, something went wrong.
		die (json_encode (array('message'=>'Problemer oppsto ved lagring av innlegget i databasen')));
?>