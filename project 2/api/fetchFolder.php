<?php 
header ("Content-type: application/json");

require_once '../include/db.php';

/** TODO: Should also consider user/public status **/

$sql = 'SELECT id, parentid, name, description from categories WHERE parentid=?';
$sth = $db->prepare ($sql);
$sth->execute (array ($_GET['parentid']));
echo json_encode ($sth->fetchAll(PDO::FETCH_ASSOC));
?>