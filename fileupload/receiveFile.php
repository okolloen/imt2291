<?php
$_apache = apache_request_headers();	// Ekstra informasjon ligger i disse, må hentes spesielt
$fn = (isset($_apache['X_FILENAME']) ? $_apache['X_FILENAME'] : false);
$mime = (isset($_apache['X_MIMETYPE']) ? $_apache['X_MIMETYPE'] : false);
$size = (isset($_apache['X_FILESIZE']) ? $_apache['X_FILESIZE'] : false);

header ("Content-type: application/json");	// Vi sender svaret som json data
if ($fn) {									// Dersom en fil er mottatt

	// AJAX call
	file_put_contents(						// Ta i mot filen og lagre den i uploads katalogen
		'uploads/' . $fn,					// Her må en ta høyde for dupliserte filnavn
		file_get_contents('php://input')
	);
	echo json_encode(array('ok'=>'OK', 'filename'=>$fn));	// Send svar til klienten
}